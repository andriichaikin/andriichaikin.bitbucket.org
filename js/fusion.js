
"use strict";

function loadData() {

	const projectIndxs = getCurrentIndxs(projectsData);
	
	updateProjectView(projectIndxs[0]);
	switchSlider('slider-indicator--portfolio', projectIndxs[0]);

	const customerIndxs = getCurrentIndxs(customerData);
	updateCustomerView(customerIndxs[0]);
	switchSlider('slider-indicator--customer', customerIndxs[0]);

	loadDefaultCards();
};


const getCurrentIndxs = function (arr) {
	const result = [];
	for (let item in arr) {
		if (arr[item].isCurrent) 
			result[result.length] = item;
	}

	return result;
}


const loadDefaultCards = function () {
	const members = team;

	const ulElements = document.getElementsByClassName('team__cards')[0];
	for (let i = 0; i < 3; i++) {

		const li = document.createElement('li');
		li.classList.add('team__card');

		const info = document.createElement('div');
		info.classList.add('team__main-info');

		const wrapper = document.createElement('div');
		wrapper.classList.add('team__photo--wrapper');

		const photo = document.createElement('img');
		photo.classList.add('team__photo');
		photo.src = members[i].path;
		photo.alt = members[i].name;;

		const name = document.createElement('p');
		name.classList.add('team__fullname');
		name.innerHTML = members[i].name;

		const position = document.createElement('p');
		position.classList.add('team__position');
		position.innerHTML = members[i].position;

		info.appendChild(wrapper);
		info.appendChild(photo);
		info.appendChild(name);
		info.appendChild(position);

		const description = document.createElement('div');
		description.classList.add('team__description');
		description.innerHTML = members[i].description;

		li.appendChild(info);
		li.appendChild(description);

		ulElements.appendChild(li);
	}
}

function moveToAnchor(sectionName) {
	if (!sectionName) return;

	const el = document.getElementsByClassName(sectionName);
	if(!el || !el[0]) return;

	el[0].scrollIntoView();

	return false;
}

/**
	used only for elements that does not have any child elemens
*/
function highlightText(sectionName) {
	if(!sectionName) return false;
	const elName = 'header__name--' + sectionName;

	const header = document.getElementsByClassName(elName);
	if(header.length === 0) return false;

	let els = document.getElementsByClassName(`${elName}-highlighted`);
	while(els.length > 0){
    	els[0].classList.remove(`${elName}-highlighted`);
	}

	const toHihglight = header[0].innerText;
	header[0].innerHTML = '';
	for (let i = 0; i < toHihglight.length; i++) {
		const inHTML = header[0].innerHTML.replace('&amp;', '&');
		const alreadyHighlighted = inHTML.length - (toHihglight.length - i);

		let result = inHTML.substring(0, alreadyHighlighted);;
		result += 
		`<span class="${elName}-highlighted">${toHihglight[i]}</span>${toHihglight.substring(i + 1)}`;

		header[0].innerHTML = result;
	}

	for(let i = 0; i < toHihglight.length; i++) {
		(function (i) {
			setTimeout(function () {
				let hlighted = document.getElementsByClassName(`${elName}-highlighted`);

				hlighted[i].style['animation-name'] = 'highlight-text';
				hlighted[i].style['animation-duration'] = '1s';

			}, 500 + (200 * i));	
		})(i);
	}

	return false;
}


function switchProject(shift) {
	const shiftedIndx = calculateShiftedNumber(shift, projectsData);
	if(shiftedIndx < 0) return false;

	updateProjectView(shiftedIndx);

	switchSlider('slider-indicator--portfolio', shiftedIndx);

	return false;
}

const updateProjectView = function (indx) {
	const project = projectsData[indx];

	const description = document.getElementsByClassName('header__description--portfolio')[0];
	description.innerHTML = project.name;

	const img = document.getElementsByClassName('portfolio__img')[0];
	img.src = project.path;
}

function switchCustomer(shift) {

	let selected = calculateShiftedNumber(shift, customerData);
	if(selected < 0) return false;

	updateCustomerView(selected);

	switchSlider('slider-indicator--customer', selected);

	return false;
}

const updateCustomerView = function (indx) {
	const customer = customerData[indx];

	const photo = document.getElementsByClassName('customer__photo')[0];
	photo.src = customer.path;

	const quote = document.getElementsByClassName('customer__quote')[0];
	quote.innerHTML = customer.quote;

	const name = document.getElementsByClassName('customer__author')[0];
	name.innerHTML = customer.name;

	const position = document.getElementsByClassName('customer__position')[0];
	position.innerHTML = customer.position;
}

function switchCards(shift) {
	const members = team;

	const prevStartFrom = [];
	for (let i = 0; i < members.length; i++) {
		if (members[i].isCurrent) {
			prevStartFrom[prevStartFrom.length] = i;
		}
	}

	let isSequential = true;
	let startIndx = 0;
	for (let i = 1; i < prevStartFrom.length; i++) {
		if (prevStartFrom[i] - prevStartFrom[i - 1] > 1) {
			isSequential = false;
			startIndx = i;
			break;
		}
	}

	let arr = isSequential ? 
		prevStartFrom : 
		[...prevStartFrom.slice(startIndx, prevStartFrom.length), ...prevStartFrom.slice(0, startIndx)];

	members.forEach(function(el) {
		el.isCurrent = false;
	});


	const ulElements = document.getElementsByClassName('team__cards')[0];
	const liElements = ulElements.getElementsByTagName('li');

	const currentIndxs = [];
	for (let i = 0; i < liElements.length; i++) {

		if (shift > 0) {
			if ((arr[i] + 1) > members.length - 1) {
				currentIndxs[i] = (members.length - 1) - arr[i];
			} else {
				currentIndxs[i] = arr[i] + 1;
			}
		} else if (shift < 0) {
			if((arr[i] - 1) < 0) {
				currentIndxs[i] = (members.length - 1);
			} else {
				currentIndxs[i] = arr[i] - 1;
			}
		}
	}

	currentIndxs.forEach(function (elIndx) {
		members[elIndx].isCurrent = true;
	});

	for(let i = 0; i < liElements.length; i++) {
		const name = liElements[i].getElementsByClassName('team__fullname')[0];
		name.innerHTML = members[currentIndxs[i]].name;;

		const photo = liElements[i].getElementsByClassName('team__photo')[0];
		photo.src = members[currentIndxs[i]].path;
		photo.alt = members[currentIndxs[i]].name;

		const position = liElements[i].getElementsByClassName('team__position')[0];
		position.innerHTML = members[currentIndxs[i]].position;

		const description = liElements[i].getElementsByClassName('team__description')[0]
		description.innerHTML = members[currentIndxs[i]].description;
	}
}

const calculateShiftedNumber = function(shift, arr) {
	let selected = -1;
	for (let i = 0; i < arr.length; i++) {
		
		if(arr[i].isCurrent) {
		
			if(shift > 0) {
				if (i === arr.length - 1) {
					selected = 0;
				} else {
					selected = i + shift;
				}	
			} else if(shift < 0) {
				if(i === 0) {
					selected = arr.length - 1;
				} else {
					selected = i + shift;
				}
			}			
			
			arr[i].isCurrent = false;
			arr[selected].isCurrent = true;

			break;
		}
	}

	return selected;
}

const switchSlider = function(clazzName, selected) {
	const ulElement = document.getElementsByClassName(clazzName)[0];

	Array.prototype.forEach.call(
			ulElement.getElementsByTagName('li'), (li, i) => {
				li.classList.remove('slider__item--active');

				if(selected == i) {
					li.classList.add('slider__item--active');
				}
			}
		)
}

const projectsData = [
		{
			'name': 'Project N1',
			'path': 'img/projects/project_0_600_400.jpg',
			'isCurrent': false,
		},
		{
			'name': 'Project N2',
			'path': 'img/projects/project_1_600_400.jpg',
			'isCurrent': false,
		},
		{
			'name': 'Project N3',
			'path': 'img/projects/project_2_600_400.jpg',
			'isCurrent': true,
		},
		{
			'name': 'Project N4',
			'path': 'img/projects/project_3_600_400.jpg',
			'isCurrent': false,
		},
		{
			'name': 'Project N5',
			'path': 'img/projects/project_4_600_400.jpg',
			'isCurrent': false,
		}
	];


const customerData = [
	{
		'name': 'Steve Jobs',
		'position': 'co-founder of Apple Computers',
		'quote': 'If you are working on something exciting that you really care about, you don’t have to be pushed. The vision pulls you',
		'path': 'img/customers/steve_jobs.jpg',
		'isCurrent': false,
	},
	{
		'name': 'Johann Wolfgang Von Goethe',
		'position': 'poet, novelist, statesman',
		'quote': 'Knowing is not enough we must apply. Wishing is not enough; we must do',
		'path': 'img/customers/goethe.jpg',
		'isCurrent': false,
	},
	{
		'name': 'Franklin D. Roosevelt',
		'position': 'statesman',
		'quote': 'The only limits to our realization of tomorrow will be our doubts of today',
		'path': 'img/customers/roosevelt.jpeg',
		'isCurrent': true,
	},
	{
		'name': 'Henry Ford',
		'position': 'founder of Ford Motor Company',
		'quote': 'Whether you think you can or think you can’t, you’re right',
		'path': 'img/customers/henry_ford.jpg',
		'isCurrent': false,
	},
	{
		'name': 'Helen Keller',
		'position': 'political activist',
		'quote': 'Optimism is the faith that leads to achievement. Nothing can be done without hope and confidence',
		'path': 'img/customers/helen_keller.jpg',
		'isCurrent': false,
	}
];

const team = [
	{
		'name': 'Bilbo Baggins',
		'path': 'img/team/bilbo.png',
		'position': 'farmer, troll killer',
		'description': 
			`Bilbo was a bearer of the One Ring and is an adoptive uncle to Frodo Baggins. 
			Though he may be small in size, Bilbo has a huge, friendly personality. 
			The tiny hobbit has had so many fantastic adventures that he eventually had to make a written account of them!`,
		'isCurrent': true,
	},
	{
		'name': 'Tyrion Lannister',
		'path': 'img/team/tyrion_lannister.jpg',
		'position': 'clever mthr fckr',
		'description': 
			`Despite Tyrion being accused for several crimes he didn’t commit, he manages to power through and escape every time. 
			He's formidable, both in intellect and in battle. 
			Tyrion doesn’t let people's opinions about him stop him, which is why we consider him to be such a strong character.`,
		'isCurrent': true,
	},
	{
		'name': 'Gandalf',
		'path': 'img/team/gandalf.jpg',
		'position': 'magician',
		'description': 
			`With a pointy hat, silvered beard, and unmatchable intellect, Gandalf can be said to be one of the greatest wizards of them all. 
			Gandalf, which literally means “Elf of the Wand,” spent some time living among Elves, learning from and teaching them, 
			until he eventually became the most powerful Istari.`,
		'isCurrent': true,
	},
	{
		'name': 'Aragorn',
		'path': 'img/team/aragorn.jpg',
		'position': 'king, warrior',
		'description': `Brave, bold, and compassionate, this Ranger of the North will always be our king.`,
		'isCurrent': false,
	},
	{
		'name': 'Temeraire',
		'path': 'img/team/temeraire.jpg',
		'position': 'poet, politician',
		'description': 
			`The Chinese Imperial dragon from Naomi Novik's book series of the same name, 
			Temeraire is a character it's impossible not to fall for. 
			An aspiring poet and politician who's unfailingly devoted to his captain Laurence, 
			Temeraire is also adamant about doing what he believes to be right, even when it's not the popular decision.`,
		'isCurrent': false,
	},
	{
		'name': 'Moiraine Damodred',
		'path': 'img/team/moiraine_damodred.jpg',
		'position': 'magician',
		'description': `A powerful member of the Aes Sedai, Moiraine's unprecedented achievements are known far and wide.`,
		'isCurrent': false,
	},
	{
		'name': 'Buffy',
		'path': 'img/team/buffy.jpg',
		'position': 'monster killer',
		'description': 
			`High school is hard. It's even harder when you're a vampire slayer living in a Hellmouth, 
			but Buffy handles her unique adolescent challenges with kick-butt prowess and immense charm.`,
		'isCurrent': false,
	},
	{
		'name': 'Shadow Moon',
		'path': 'img/team/shadow_moon.jpg',
		'position': 'bodyguard',
		'description': 
			`The ex-con protagonist of Neil Gaiman's urban fantasy book American Gods, 
			Shadow Moon is the coolest guy around—heck, he's literally Odin's bodyguard. What's cooler than that?`,
		'isCurrent': false,
	},
	{
		'name': 'Samwise Gamgee',
		'path': 'img/team/samwise_gamgee.jpg',
		'position': 'team player',
		'description': 
			`We all wish we could have a loyal friend like Samwise Gamgee. 
			Without Sam's loyalty and courage, the One Ring would never have been destroyed.`,
		'isCurrent': false,
	},
];